﻿namespace EF_CODE_FIRST.Models
{
    public class Accounts
    {
        public int ID { get; set; }
        public int CustomerID { get; set; } 
        public Customer? Customer { get; set; }

        public string? AccountName { get; set; }
    }
}
