﻿using Microsoft.EntityFrameworkCore;
namespace EF_CODE_FIRST.Models
{

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
        options) : base(options){ }
        

        public DbSet<Employees> employyee { get; set; }

        public DbSet<Customer> customer { get; set; }

        public DbSet<Accounts> account { get; set; }

        public DbSet<Reports> report { get; set; }

        public DbSet<Logs> log { get; set; }
        public DbSet<Transactions> transaction { get; set; }

    }
}
