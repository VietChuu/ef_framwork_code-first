﻿namespace EF_CODE_FIRST.Models
{
    public class Transactions
    {
        public int ID { get; set; }

        public int EmployeesID { get; set; }
        public Employees? Employees { get; set; }

        public int CustomerId { get; set; }
        public Customer? Customer { get; set; }

        public string? TransactionName { get; set; }
    }
}
