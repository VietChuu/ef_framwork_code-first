﻿namespace EF_CODE_FIRST.Models
{
    public class Employees
    {
        public int ID { get; set; }
        public string? FristName { get; set; }

        public string? LastName { get; set; }

        public string? ContactAndAddress { get; set; }
        public string? Username{ get; set; }
        public string? Password { get; set; }
    }
}
