﻿namespace EF_CODE_FIRST.Models
{
    public class Logs
    {
        public int ID { get; set; }

        public int TransactionsID { get; set; } 

        public DateTime LoginDate { get; set; }

        public DateTime LoginTime { get; set;}
    }
}
 