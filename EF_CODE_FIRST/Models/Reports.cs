﻿namespace EF_CODE_FIRST.Models
{
    public class Reports
    {
        public int ID { get; set; }

        public int AccountID { get; set; } 
  
        
        public int LogsID { get; set; }
        public Logs? Logs { get; set; }

        public Transactions? Transactions { get; set; }

        public string? ReportName { get; set; }

        public DateTime ReportDate { get; set; }
    }
}
