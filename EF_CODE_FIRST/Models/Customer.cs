﻿namespace EF_CODE_FIRST.Models
{
    public class Customer
    {
        public int ID { get; set; }
        public string? CustomerFName { get; set; }

        public string? CustomerLName { get; set; }

        public string? CustomerContactAdrr { get; set; }

        public string? CustomerUsername { get; set; }

        public string? CustomerPassword { get; set; }

    }
}
